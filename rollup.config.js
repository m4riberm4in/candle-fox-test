import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import alias from 'rollup-plugin-alias';
import scss from 'rollup-plugin-scss';
import postcss from 'rollup-plugin-postcss';
import analyze from 'rollup-plugin-analyzer';
import autoPreprocess from 'svelte-preprocess';
import image from '@rollup/plugin-image';
import url from '@rollup/plugin-url';

const production = !process.env.ROLLUP_WATCH;

export default {
	input: 'src/main.js',
	output: [
		{
			sourcemap: true,
			format: 'cjs',
			name: 'bundle',
			file: 'public/bundle-cjs.js',
		},
		{
			sourcemap: true,
			format: 'iife',
			name: 'bundle',
			file: 'public/bundle.js',
			plugins: [terser()]
		},
		{
			sourcemap: true,
			format: 'umd',
			name: 'bundle',
			file: 'public/bundle-umd.js',
		}
	],
	plugins: [
		url(),
		image(),
		scss({
			//Choose *one* of these possible "output:..." options
			// Default behaviour is to write all styles to the bundle destination where .js is replaced by .css
			output: true,
		  
			// Filename to write all styles to
			output: 'bundle.css',
		  
			// Callback that will be called ongenerate with two arguments:
			// - styles: the contents of all style tags combined: 'body { color: green }'
			// - styleNodes: an array of style objects: { filename: 'body { ... }' }
			output: function (styles, styleNodes) {
			  writeFileSync('bundle.css', styles)
			},
		  
			// Disable any style output or callbacks, import as string
			output: true,
		  
			// Determine if node process should be terminated on error (default: false)
			failOnError: true,
		  
			// Prefix global scss. Useful for variables and mixins.
			prefix: `@import "src/styles/_mixins.scss";`
		  }),
		  postcss({
			extract: true,
			minimize: true,
			use: [
			  ['sass', {
				includePaths: [
				  './src/styles',
				  './node_modules'
				]
			  }]
			]
		  }),
		alias({
			resolve: ['.jsx', '.js'],
			entries:[
			  {find:'@Styles', replacement: 'src/styles'},
			  {find:'@Components', replacement: 'src/components'},
			  {find:'@Assets', replacement: 'src/assets'},
			]
		}),
		svelte({
			// enable run-time checks when not in production
			dev: !production,
			// we'll extract any component CSS out into
			// a separate file - better for performance
			css: css => {
				css.write('public/bundle.css');
			},
			preprocess: autoPreprocess({
				postcss: true
			}),
			emitCss: false
		}),

		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration -
		// consult the documentation for details:
		// https://github.com/rollup/plugins/tree/master/packages/commonjs
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
		analyze({
			summaryOnly: true
		}),
		// strip(),
		// In dev mode, call `npm run start` once
		// the bundle has been generated
		!production && serve(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		!production && livereload('public'),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser({
			compress: {
				inline: 1,
				drop_console: production,
			},

		})
	],
	watch: {
		clearScreen: false
	}
};

function serve() {
	let started = false;

	return {
		writeBundle() {
			if (!started) {
				started = true;

				require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
					stdio: ['ignore', 'inherit', 'inherit'],
					shell: true
				});
			}
		}
	};
}
