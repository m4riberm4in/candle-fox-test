const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

const mode = process.env.NODE_ENV || 'development';
const prod = mode === 'production';

const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const isInline = process.env.inline;

module.exports = {
	entry: {
		bundle: ['./src/main.js']
	},
	resolve: {
		alias: {
			svelte: path.resolve('node_modules', 'svelte'),
			'@Components': path.resolve(__dirname, 'src/components/'),
			'@Services': path.resolve(__dirname, 'src/services/'),
			'@Assets': path.resolve(__dirname, 'src/assets/'),
			'@Constant': path.resolve(__dirname, 'src/constant/'),
			'@Styles': path.resolve(__dirname, 'src/styles/'),
			'@Stores': path.resolve(__dirname, 'src/stores/'),
			'@Helpers': path.resolve(__dirname, 'src/helpers/'),
			'@Containers': path.resolve(__dirname, 'src/containers/'),

		},
		extensions: ['.mjs', '.js', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
	// output: {
	// 	path: __dirname + '/public',
	// 	filename: '[name].js',
	// 	chunkFilename: '[name].[id].js'
	// },
  // output file(s) and chunks
	output: {
		library: 'BubbleQ',
		libraryTarget: 'umd',
		libraryExport: 'default',
		path: __dirname + '/public',
		filename: 'bubbleq.js',
		umdNamedDefine: true,
	},	
	optimization: {
		minimize: true,
		minimizer: [
			new TerserPlugin({
			  sourceMap: true, // Must be set to true if using source-maps in production
			  terserOptions: {
				compress: {
				  drop_console: prod ? true : false,
				},
			  },
			}),
		  ],
		// splitChunks: {
		// 	cacheGroups: {
		// 		node_vendors : {
		// 			test: /[\\/]node_modules[\\/]/,
		// 			chunks: "all",
		// 			priority: 1,
		// 		}
		// 	}
		// }
	},
	module: {
		rules: [
			{
				test: /\.(js|mjs|svelte)$/,
				exclude: /node_modules\/(?!svelte)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
						plugins: ['@babel/plugin-proposal-object-rest-spread']
					}
				},
			},
			{
				test: /\.svelte$/,
				exclude: /node_modules/,
				use: {
					loader: 'svelte-loader',
					options: {
						emitCss: true,
						hotReload: true,
					}
				}
			},
			{
				test: /\.css$/,
				use: [
					/**
					 * MiniCssExtractPlugin doesn't support HMR.
					 * For developing, use 'style-loader' instead.
					 * */
					prod ? MiniCssExtractPlugin.loader : 'style-loader',
					'css-loader',
					"postcss-loader"
				]
			},
			{
				test: /\.(png|jpe?g|gif|mp4|svg|ttf)$/,
				use: [
					{
						loader: 'file-loader'
					},
				],
			},
			{
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
			  },
		]
	},
	mode,
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css'
		}),
		new HtmlWebpackPlugin({
			inlineSource: isInline ? '.(js|css)$' : '',
			hash: isInline ? false : true,
			template: './src/index.html',
			title: 'SehatQ Chat Widget'
		}),
		new HtmlWebpackInlineSourcePlugin(),
	],
	devtool: prod ? false : 'source-map',
	devServer: {
		port: 3000
	}
};
